module Api
  module V1
    class ProductsController < ApplicationController
      before_action :set_product, only: [:show]

      def index
        products = Product.all
        render json: products, status: :ok
      end

      def create
        product = Product.new(prod_params)
        if product.save
          render json: product, status: :created
        else
          render json: { error: 'Error creating product.' }, status: :unprocessable_entity
        end
      end

      def show
        render json: @product, status: :ok
      end

      private

      def set_product
        @product = Product.find_by(id: params[:id])
        return if @product

        render json: { error: 'Product Not Found' }, status: :not_found
      end

      def prod_params
        params.require(:product).permit(
          :name,
          :brand,
          :price,
          :description
        )
      end
    end
  end
end
