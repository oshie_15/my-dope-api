class Product < ApplicationRecord
  validates :name, presence: true, length: { minimum: 3, maximum: 100 }
  validates :brand, presence: true, length: { minimum: 3, maximum: 100 }
  validates :price, presence: true, length: { minimum: 3, maximum: 100 }
  validates :description, presence: true, length: { minimum: 3, maximum: 100 }
end
