<!-- PROJECT DESCRIPTION -->

# & Steps to create an API<a name="about-project"></a>

**Creating API** & Steps creating an API simple web application. User can get, create, update, delete, menu items.
Built with ruby on rails and PostgreSQL

## 🛠 Built With <a name="built-with">Ruby, Ruby on Rails, Docker, PostgreSQL</a>

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://rubyonrails.org/">Ruby on Rails</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.sqlite.org/">PostgreSQL</a></li>
  </ul>
</details>

### Key Features <a name="key-features"></a>

- **CRUD task by using HTTP resquests**

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

```sh
 gem install rails
```

```sh
 Docker
```

```sh
 PostgreSQL
```


### Setup

Clone this repository to your desired folder:

```sh
  git clone git@gitlab.com:oshie_15/my-dope-api.git

```

```sh
  cd my-dope-api
  and switch to the correct branch name `my_feature`
```
```sh
  Create your own .env file 
```
```sh
  Enter your DATABASE_USER, DATABASE_PASSWORD
```


### Usage

To run the project, execute the following command:

```sh
  docker-compose build
```

```sh
  docker-compose up -d
```

### Install

```sh
  docker-compose exec app rails db:create
```
```sh
  docker-compose exec app rails db:migrate
```

```sh
  docker-compose exec app rails db:seed
```

## 👥 Authors <a name="Oshie" />

👤 **Oshie**

- GitLab: [GitLab Profile](https://gitlab.com/oshie_15)
