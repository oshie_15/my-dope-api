# Use an official Ruby runtime as the base image
FROM ruby:3.2.2

# Set the working directory in the container
WORKDIR /app

# Copy the Gemfile and Gemfile.lock into the container
COPY Gemfile Gemfile.lock ./

# Install dependencies
RUN bundle install

# Copy the rest of the application code into the container
COPY . .

# Set the command to start the Rails application
CMD ["rails", "server", "-b", "0.0.0.0"]