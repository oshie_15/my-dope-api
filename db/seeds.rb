# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Product.create([
                 {
                   name: 'Quite Comfort 35',
                   brand: 'Bose',
                   price: '$279',
                   description: 'Wireless Bluetooth Headphone'
                 },
                 {
                   name: 'Noise Cancelling Headphone',
                   brand: 'Sony',
                   price: '$348',
                   description: 'Wireless Bluetooth Headphone with Mic'
                 },
                 {
                   name: 'Freedom Aluminium Patio Slading Glass Pet Door',
                   brand: 'PetSafe',
                   price: '$130',
                   description: 'Sliding Glass Pet Door for Dogs and Cats'
                 }
               ])

puts 'Seed data has been created.'
